#!/bin/env node

const objson = {
  "opml": {
    "body": {
      "outline": [
        {
          "outline": [
            {
              "@attributes": {
                "col": "1",
                "row": "562.5",
                "type": "rss",
                "htmlUrl": "https://www.journalduhacker.net/",
                "xmlUrl": "https://www.journalduhacker.net/rss",
                "title": "RSS 2.0"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "1562.5",
                "type": "rss",
                "htmlUrl": "https://sebsauvage.net/links/",
                "xmlUrl": "https://sebsauvage.net/links/?do=rss",
                "title": "RSS Feed"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "2562.5",
                "type": "rss",
                "htmlUrl": "https://www.cieletespace.fr/",
                "xmlUrl": "https://www.cieletespace.fr/rss.xml",
                "title": "Ciel &amp; Espace"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://korben.info/",
                "xmlUrl": "https://korben.info/feed",
                "title": "Korben"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "https://www.numerama.com/",
                "xmlUrl": "https://www.numerama.com/feed/rss/",
                "title": "Numerama"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "3000",
                "type": "rss",
                "htmlUrl": "http://www.uptimerobot.com/",
                "xmlUrl": "https://rss.uptimerobot.com/u1141176-24c3da691fc22bc99119c98d188b5593",
                "title": "Uptime Robot - All Monitors"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "4000",
                "type": "rss",
                "htmlUrl": "https://www.libre.re/latest",
                "xmlUrl": "https://www.libre.re/latest.rss",
                "title": "Libre.re - Sujets récents"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "500",
                "type": "rss",
                "htmlUrl": "https://www.humanite.fr/",
                "xmlUrl": "https://www.humanite.fr/rss.xml",
                "title": "L'actualité par l'Humanite.fr"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://www.liberation.fr/",
                "xmlUrl": "http://rss.liberation.fr/rss/latest/",
                "title": "A la une sur Libération"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "https://theconversation.com/",
                "xmlUrl": "https://theconversation.com/fr/home-page/articles.atom",
                "title": "Page d'accueil – analyses"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "250",
                "type": "rss",
                "htmlUrl": "https://lemediatv.fr/",
                "xmlUrl": "https://api.lemediatv.fr/rss.xml",
                "title": "Flux RSS | Le Média"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "750",
                "type": "rss",
                "htmlUrl": "https://www.lemonde.fr/rss/une.xml",
                "xmlUrl": "https://www.lemonde.fr/rss/une.xml",
                "title": "Le Monde.fr - Actualités et Infos en France et dans le monde"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "1750",
                "type": "rss",
                "htmlUrl": "https://theconversation.com/",
                "xmlUrl": "https://theconversation.com/fr/home-page/news.atom",
                "title": "Page d'accueil - Recherche et Infos"
              }
            }
          ],
          "#text": "",
          "@attributes": {
            "layout": "4-0",
            "cols": "4",
            "title": "General"
          }
        },
        {
          "outline": [
            {
              "@attributes": {
                "col": "1",
                "row": "500",
                "type": "rss",
                "htmlUrl": "https://linuxfr.org/news",
                "xmlUrl": "http://linuxfr.org/news.atom",
                "title": "Flux Atom des dépêches"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "5000",
                "type": "rss",
                "htmlUrl": "https://15issd.tumblr.com/",
                "xmlUrl": "http://15issd.tumblr.com/rss.xml",
                "title": "15ISSD"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "7000",
                "type": "rss",
                "htmlUrl": "http://www.berkeleymews.com/",
                "xmlUrl": "http://www.berkeleymews.com/?feed=rss2",
                "title": "Berkeley Mews RSS2 Feed"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "7500",
                "type": "rss",
                "htmlUrl": "https://www.commitstrip.com/",
                "xmlUrl": "http://www.commitstrip.com/fr/feed/",
                "title": "CommitStrip"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "7750",
                "type": "rss",
                "htmlUrl": "https://yatuu.fr/",
                "xmlUrl": "http://yatuu.fr/feed/",
                "title": "YATUU - blog BD d'une ex-stagiaire"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "8000",
                "type": "rss",
                "htmlUrl": "http://levilainblog.blogspot.com/",
                "xmlUrl": "http://levilainblog.blogspot.com/feeds/posts/default",
                "title": "Des Verges et des Corbeaux - Atom"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "9000",
                "type": "rss",
                "htmlUrl": "https://entoutesimplicitude.tumblr.com/",
                "xmlUrl": "http://entoutesimplicitude.tumblr.com/rss",
                "title": "En toute simplicitude"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "10000",
                "type": "rss",
                "htmlUrl": "https://james-o-rama.tumblr.com/",
                "xmlUrl": "http://james-o-rama.tumblr.com/rss.xml",
                "title": "James-o-rama"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "11000",
                "type": "rss",
                "htmlUrl": "https://projetcrocodiles.tumblr.com/",
                "xmlUrl": "http://projetcrocodiles.tumblr.com/rss",
                "title": "Projet Crocodiles"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "12000",
                "type": "rss",
                "htmlUrl": "https://oglaf.lapin.org/",
                "xmlUrl": "http://oglaf.lapin.org/fluxrss.xml",
                "title": "Oglaf"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "14000",
                "type": "rss",
                "htmlUrl": "https://www.lemonde.fr/blog/lavventura",
                "xmlUrl": "http://lavventura.blog.lemonde.fr/feed/",
                "title": "L'Avventura"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "15000",
                "type": "rss",
                "htmlUrl": "http://www.urtikan.net/",
                "xmlUrl": "http://www.urtikan.net/feed/",
                "title": "Urtikan.net"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "17000",
                "type": "rss",
                "htmlUrl": "https://hackstock.net/",
                "xmlUrl": "https://hackstock.net/podcast.xml",
                "title": "hackstock"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "18000",
                "type": "rss",
                "htmlUrl": "http://clementfabre.fr/blog",
                "xmlUrl": "http://clementfabre.fr/blog/feed/",
                "title": "Le blog de Clément C. Fabre"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "22000",
                "type": "rss",
                "htmlUrl": "http://www.zviane.com/prout",
                "xmlUrl": "http://www.zviane.com/prout/feed",
                "title": "La plus jolie fin du monde » Flux"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "23000",
                "type": "rss",
                "htmlUrl": "http://blog.chabd.com/",
                "xmlUrl": "http://blog.chabd.com/abonnement.xml",
                "title": "Ma vie est une bande dessinée"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "25000",
                "type": "rss",
                "htmlUrl": "http://elosterv.blogspot.com/",
                "xmlUrl": "http://elosterv.blogspot.com/feeds/posts/default",
                "title": "Elosterv - Atom"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "28000",
                "type": "rss",
                "htmlUrl": "http://macadamvalley.com/",
                "xmlUrl": "http://macadamvalley.com/feed/",
                "title": "Macadam Valley » Feed"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "29000",
                "type": "rss",
                "htmlUrl": "http://www.yodablog.net/",
                "xmlUrl": "http://www.yodablog.net/?feed=rss2",
                "title": "RSS 2.0 - Tous les articles"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "31000",
                "type": "rss",
                "htmlUrl": "https://von.zone/",
                "xmlUrl": "https://blog.tomcelestin.me/rss/",
                "title": "tomcelestin"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "32000",
                "type": "rss",
                "htmlUrl": "http://blogs.lesinrocks.com/gimmeindierock",
                "xmlUrl": "http://blogs.lesinrocks.com/gimmeindierock/feed/",
                "title": "Gimme Indie Rock » Flux"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "33000",
                "type": "rss",
                "htmlUrl": "http://zacdeloupy.over-blog.com/",
                "xmlUrl": "http://zacdeloupy.over-blog.com/rss",
                "title": "Flux RSS des articles"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "36500",
                "type": "rss",
                "htmlUrl": "http://beux.over-blog.com/",
                "xmlUrl": "http://beux.over-blog.com/rss-articles.xml",
                "title": "sans titre"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "37000",
                "type": "rss",
                "htmlUrl": "",
                "xmlUrl": "http://www.gillesbabinet.com/cms/feed/",
                "title": "Gilles Babinet"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "40000",
                "type": "rss",
                "htmlUrl": "http://yap-yap-yap-yap.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/2997637686638134064/posts/default",
                "title": "yap"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "43000",
                "type": "rss",
                "htmlUrl": "http://auroyaumedesaveugles.8comix.fr/index.php?",
                "xmlUrl": "http://auroyaumedesaveugles.8comix.fr/index.php?feed/rss2",
                "title": "AU ROYAUME DES AVEUGLES - Salsedo &#x2F; Jouvray &#x2F; Salsedo"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "44000",
                "type": "rss",
                "htmlUrl": "http://eliascarpe.over-blog.com/",
                "xmlUrl": "http://eliascarpe.over-blog.com/rss-articles.xml",
                "title": "Eliascarpe"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "47000",
                "type": "rss",
                "htmlUrl": "https://www.leburp.com/",
                "xmlUrl": "http://www.leburp.com/feed/",
                "title": "La formidable existence du burp"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "62.5",
                "type": "rss",
                "htmlUrl": "https://maliki.com/",
                "xmlUrl": "http://maliki.com/strips/feed/",
                "title": "MALIKI - Webcomic » Flux pour Strips"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "250",
                "type": "rss",
                "htmlUrl": "https://bloglaurel.com/",
                "xmlUrl": "http://www.bloglaurel.com/feed",
                "title": "RSS"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "375",
                "type": "rss",
                "htmlUrl": "http://www.bouletcorp.com/",
                "xmlUrl": "http://www.bouletcorp.com/feed/",
                "title": "bouletcorp"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "http://blogmotion.fr/",
                "xmlUrl": "http://blogmotion.fr/feed",
                "title": "Blogmotion"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "https://h5ckfun.info/",
                "xmlUrl": "http://h5ckfun.info/feed/",
                "title": "H5ckFun » Flux"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "2500",
                "type": "rss",
                "htmlUrl": "http://lemonde2images.blogspot.com/",
                "xmlUrl": "http://lemonde2images.blogspot.com/feeds/posts/default",
                "title": "Le monde en 1 ou 2 images...ou plus ! - Atom"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "3000",
                "type": "rss",
                "htmlUrl": "http://adaetrosie.blogs.liberation.fr/",
                "xmlUrl": "http://adaetrosie.blogs.liberation.fr/feeds/",
                "title": "Ada et Rosie"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "5000",
                "type": "rss",
                "htmlUrl": "http://bambiiiblog.blogspot.com/",
                "xmlUrl": "http://bambiiiblog.blogspot.com/feeds/posts/default",
                "title": "Bambiii Blog - Atom"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "6000",
                "type": "rss",
                "htmlUrl": "http://terreur-graphique.blogs.liberation.fr/",
                "xmlUrl": "http://terreur-graphique.blogs.liberation.fr/feeds/",
                "title": "Terreur Graphique"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "7000",
                "type": "rss",
                "htmlUrl": "http://wertch.blogspot.com/",
                "xmlUrl": "http://wertch.blogspot.com/feeds/posts/default",
                "title": "Wertch. - Atom"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "7500",
                "type": "rss",
                "htmlUrl": "https://www.lemonde.fr/blog/vidberg",
                "xmlUrl": "http://vidberg.blog.lemonde.fr/feed/",
                "title": "L'actu en patates"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "8000",
                "type": "rss",
                "htmlUrl": "http://nepsie.fr/leblog",
                "xmlUrl": "http://nepsie.fr/leblog/feed/",
                "title": "Le blog d'une gentille » Flux"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "10000",
                "type": "rss",
                "htmlUrl": "https://lewistrondheim.tumblr.com/",
                "xmlUrl": "http://lewistrondheim.tumblr.com/rss",
                "title": "...tiroirs..."
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "10500",
                "type": "rss",
                "htmlUrl": "https://www.paka-blog.com/",
                "xmlUrl": "http://www.paka-blog.com/feed/",
                "title": "Paka Blog BD | Humour Noir | Ineptie Humoristique |"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "10750",
                "type": "rss",
                "htmlUrl": "http://ultimex.over-blog.com/",
                "xmlUrl": "http://ultimex.over-blog.com/rss-articles.xml",
                "title": "Ultimex et Steve le faire-valoir prodige"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "11000",
                "type": "rss",
                "htmlUrl": "http://diglee.com/",
                "xmlUrl": "http://diglee.com/feed/",
                "title": "Diglee | Mon blog d'illustratrice"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "12500",
                "type": "rss",
                "htmlUrl": "http://www.delitoon.comhttp://www.delitoon.com/serie-webtoon/serie-madie.html",
                "xmlUrl": "http://www.delitoon.com/serie-webtoon/serie-madie.feed",
                "title": "Delitoon - Madie"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "14000",
                "type": "rss",
                "htmlUrl": "https://geekologie.com/",
                "xmlUrl": "http://www.geekologie.com/index.xml",
                "title": "Geekologie - Gadgets, Gizmos, and Awesome"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "14500",
                "type": "rss",
                "htmlUrl": "http://jap-jap-jap-jap.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/6224709927020886777/posts/default",
                "title": "La réception bonjour"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "15000",
                "type": "rss",
                "htmlUrl": "http://www.melakarnets.com/index.php",
                "xmlUrl": "http://www.melakarnets.com/index.php?feed/atom",
                "title": "Mélakarnets"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "16000",
                "type": "rss",
                "htmlUrl": "http://maadiar.blogspot.com/",
                "xmlUrl": "http://maadiar.blogspot.com/feeds/posts/default",
                "title": "Maadiar &#x2F; Маадяр Blog Blog - Atom"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "21000",
                "type": "rss",
                "htmlUrl": "http://lesmadeleinesdemady.com/",
                "xmlUrl": "http://www.lesmadeleinesdemady.com/rss-articles.xml",
                "title": "Les Madeleines de Mady"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "25000",
                "type": "rss",
                "htmlUrl": "http://www.dreamy.fr/blog",
                "xmlUrl": "http://feeds.feedburner.com/Nani-Nani",
                "title": "Nani Nani ~ le blog de Dreamy"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "27000",
                "type": "rss",
                "htmlUrl": "https://pbfcomics.com/",
                "xmlUrl": "http://pbfcomics.com/feed/feed.xml",
                "title": "The Perry Bible Fellowship"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "30000",
                "type": "rss",
                "htmlUrl": "http://donne-moi-ton-ballon.blogspot.com/",
                "xmlUrl": "http://donne-moi-ton-ballon.blogspot.com/feeds/posts/default?alt=rss",
                "title": "La Dissonance des Corps"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "31500",
                "type": "rss",
                "htmlUrl": "http://www.juliemaroh.com/",
                "xmlUrl": "http://www.juliemaroh.com/feed/",
                "title": "Les coeurs exacerbés"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "34000",
                "type": "rss",
                "htmlUrl": "http://krrpk.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/8766641781072880575/posts/default",
                "title": "krrpk"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "41000",
                "type": "rss",
                "htmlUrl": "http://blog.zanorg.com/",
                "xmlUrl": "http://blog.zanorg.com/rss/fil_rss.xml",
                "title": ".chez kek."
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "125",
                "type": "rss",
                "htmlUrl": "https://www.grafikart.fr/",
                "xmlUrl": "http://feeds.feedburner.com/Grafikart",
                "title": "Grafikart"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "187.5",
                "type": "rss",
                "htmlUrl": "https://hackaday.com/",
                "xmlUrl": "http://feeds2.feedburner.com/hackaday/LgoM",
                "title": "Hack a Day"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "250",
                "type": "rss",
                "htmlUrl": "https://lepressoir-info.org/",
                "xmlUrl": "http://lepressoir-info.org/spip.php?page=backend",
                "title": "Syndiquer tout le site"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "500",
                "type": "rss",
                "htmlUrl": "http://lesculottees.blog.lemonde.fr/",
                "xmlUrl": "http://lesculottees.blog.lemonde.fr/feed/",
                "title": "Les Culottées » Flux"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://www.undernews.fr/",
                "xmlUrl": "http://www.undernews.fr/feed/atom",
                "title": "Atom 0.3"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "http://zepworld.blog.lemonde.fr/",
                "xmlUrl": "http://zepworld.blog.lemonde.fr/feed/",
                "title": "What a wonderful world » Flux"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "2500",
                "type": "rss",
                "htmlUrl": "http://www.obion.fr/blog",
                "xmlUrl": "http://www.obion.fr/blog/feed/",
                "title": "Le blog d'Obion"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "6000",
                "type": "rss",
                "htmlUrl": "https://davidsnugblog.wordpress.com/",
                "xmlUrl": "http://davidsnugblog.wordpress.com/feed/",
                "title": "David Snug Blog » Flux"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "7000",
                "type": "rss",
                "htmlUrl": "https://terreurgraphique.tumblr.com/",
                "xmlUrl": "http://terreurgraphique.tumblr.com/rss",
                "title": "Terreur Graphique"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "8000",
                "type": "rss",
                "htmlUrl": "http://www.delitoon.comhttp://www.delitoon.com/serie-webtoon/serie-perkeros.html",
                "xmlUrl": "http://www.delitoon.com/serie-webtoon/serie-perkeros.feed",
                "title": "Delitoon - Perkeros"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "9000",
                "type": "rss",
                "htmlUrl": "http://elblogdejoancornella.blogspot.com/",
                "xmlUrl": "http://elblogdejoancornella.blogspot.com/feeds/posts/default",
                "title": "Joan Cornellà"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "10000",
                "type": "rss",
                "htmlUrl": "http://leblogamalec.blogspot.com/",
                "xmlUrl": "http://leblogamalec.blogspot.com/feeds/posts/default",
                "title": "LE BLOG A MALEC"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "12000",
                "type": "rss",
                "htmlUrl": "https://ulricabrac.wordpress.com/",
                "xmlUrl": "http://ulricabrac.wordpress.com/feed/",
                "title": "Ulric à brac"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "13000",
                "type": "rss",
                "htmlUrl": "http://www.delitoon.comhttp://www.delitoon.com/espace-reperage-series/serie-sauvage-ou-la-sagesse-des-pierres.html",
                "xmlUrl": "http://www.delitoon.com/espace-reperage-series/serie-sauvage-ou-la-sagesse-des-pierres.feed",
                "title": "SAUVAGE, ou la sagesse des pierres."
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "14000",
                "type": "rss",
                "htmlUrl": "http://aufonddutrou.fr/",
                "xmlUrl": "http://feeds.feedburner.com/AuFondDuTrou",
                "title": "Au fond du trou"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "18000",
                "type": "rss",
                "htmlUrl": "http://www.leamlu.com/index.php",
                "xmlUrl": "http://www.leamlu.com/index.php?feed/rss2",
                "title": "Leamlu's Stuff"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "21000",
                "type": "rss",
                "htmlUrl": "http://charliepoppins.blogspot.com/",
                "xmlUrl": "http://feeds.feedburner.com/CharliePoppinsScrapbook",
                "title": "Charlie Poppins"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "23000",
                "type": "rss",
                "htmlUrl": "http://commedesguilis.blogspot.com/",
                "xmlUrl": "http://feeds.feedburner.com/CommeDesGuilisDansLeBasDuVentre",
                "title": "Comme des guilis dans le bas du ventre..."
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "24000",
                "type": "rss",
                "htmlUrl": "http://www.monsieur-le-chien.fr/",
                "xmlUrl": "http://www.monsieur-le-chien.fr/rss.php",
                "title": "Monsieur Le Chien"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "26000",
                "type": "rss",
                "htmlUrl": "http://tumourrasmoinsbete.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/1879199009612029553/posts/default",
                "title": "Tu mourras moins bête"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "27000",
                "type": "rss",
                "htmlUrl": "http://berth.canalblog.com/",
                "xmlUrl": "http://berth.canalblog.com/rss.xml",
                "title": "C'est facile de se moquer"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "28000",
                "type": "rss",
                "htmlUrl": "https://www.lemonde.fr/blog/long",
                "xmlUrl": "http://long.blog.lemonde.fr/feed/",
                "title": "A boire et à manger"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "30000",
                "type": "rss",
                "htmlUrl": "http://blaguiblago.over-blog.com/",
                "xmlUrl": "http://www.blaguiblago.com/rss-articles.xml",
                "title": "Le blog de blaguiblago.com"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "31000",
                "type": "rss",
                "htmlUrl": "http://blogdepluche.over-blog.net/",
                "xmlUrl": "http://blogdepluche.over-blog.net/rss-articles.xml",
                "title": "Un blog de pluche"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "32000",
                "type": "rss",
                "htmlUrl": "http://saboten.canalblog.com/",
                "xmlUrl": "http://saboten.canalblog.com/rss.xml",
                "title": "L'hypopothèque"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "35000",
                "type": "rss",
                "htmlUrl": "http://www.jeromeuh.net/",
                "xmlUrl": "http://www.jeromeuh.net/feed/",
                "title": "Le blog de Jeromeuh"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "37000",
                "type": "rss",
                "htmlUrl": "https://maesterbd.wordpress.com/",
                "xmlUrl": "http://maesterbd.wordpress.com/feed/",
                "title": "La Grande Tambouille de Maëster"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "38000",
                "type": "rss",
                "htmlUrl": "http://stoufetjeanouf.canalblog.com/",
                "xmlUrl": "http://www.stoufetjeanouf.net/rss.xml",
                "title": "Stouf et Jean-Ouf"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "39000",
                "type": "rss",
                "htmlUrl": "http://zelba.over-blog.com/",
                "xmlUrl": "http://zelba.over-blog.com/rss-articles.xml",
                "title": "Le blog de Zelba"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "125",
                "type": "rss",
                "htmlUrl": "https://cereales.lapin.org/",
                "xmlUrl": "http://cereales.lapin.org/fluxrss.xml",
                "title": "Les Céréales du Dimanche Matin"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "500",
                "type": "rss",
                "htmlUrl": "http://sametmax.com/",
                "xmlUrl": "http://sametmax.com/feed/",
                "title": "Sam &amp; Max"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "http://www.piratesourcil.com/",
                "xmlUrl": "http://piratesourcil.blogspot.com/feeds/posts/default",
                "title": "PirateSourcil - Atom"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "https://blog.genma.fr/",
                "xmlUrl": "https://blog.genma.fr/spip.php?page=backend",
                "title": "RSS - Blog de Genma"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "5000",
                "type": "rss",
                "htmlUrl": "http://gallybox.com/blog",
                "xmlUrl": "http://gallybox.com/blog/feed/",
                "title": "Gally blog » Flux"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "6000",
                "type": "rss",
                "htmlUrl": "http://poubellededav.blogspot.com/",
                "xmlUrl": "http://poubellededav.blogspot.com/feeds/posts/default",
                "title": "la poubelle de dav - Atom"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "7000",
                "type": "rss",
                "htmlUrl": "http://isacile.blogspot.com/",
                "xmlUrl": "http://isacile.blogspot.com/feeds/posts/default",
                "title": "isacile."
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "8000",
                "type": "rss",
                "htmlUrl": "https://loicsecheresse.tumblr.com/",
                "xmlUrl": "http://loicsecheresse.tumblr.com/rss.xml",
                "title": "Yault."
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "9000",
                "type": "rss",
                "htmlUrl": "http://chezdoyen.canalblog.com/",
                "xmlUrl": "http://chezdoyen.canalblog.com/rss.xml",
                "title": "Chez doyen"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "10000",
                "type": "rss",
                "htmlUrl": "http://www.mirionmalle.com/",
                "xmlUrl": "http://www.mirionmalle.com/feeds/posts/default",
                "title": "Commando Culotte || le blog de Mirion Malle"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "11000",
                "type": "rss",
                "htmlUrl": "http://gloryowlcomix.blogspot.com/",
                "xmlUrl": "http://gloryowlcomix.blogspot.com/feeds/posts/default",
                "title": "  GLORY OWL"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "22000",
                "type": "rss",
                "htmlUrl": "http://bastienvives.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/1216442419974548527/posts/default",
                "title": "comme quoi"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "27000",
                "type": "rss",
                "htmlUrl": "",
                "xmlUrl": "https://www.franceinter.fr/emissions/le-moment-meurice",
                "title": "La Lanterne Brisée"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "33000",
                "type": "rss",
                "htmlUrl": "http://www.pochep.fr/",
                "xmlUrl": "http://www.pochep.fr/rss-articles.xml",
                "title": "Politburo by Pochep"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "37000",
                "type": "rss",
                "htmlUrl": "http://lestoujoursouvrables.fr/",
                "xmlUrl": "http://lestoujoursouvrables.fr/feed/",
                "title": "Les toujours ouvrables"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "38000",
                "type": "rss",
                "htmlUrl": "http://chezcalice.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/3925267312150248648/posts/default",
                "title": "Chez Calice"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "39000",
                "type": "rss",
                "htmlUrl": "http://billotomo.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/7165924328602135476/posts/default",
                "title": "bill otomo"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "47000",
                "type": "rss",
                "htmlUrl": "http://mikesquadventures.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/2963766599679914572/posts/default",
                "title": "Mikesquad'ventures"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "49000",
                "type": "rss",
                "htmlUrl": "http://www.chez-barbu.com/",
                "xmlUrl": "http://feeds.feedburner.com/ChezBarbu",
                "title": "Chez Barbu - Blog BD"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "51000",
                "type": "rss",
                "htmlUrl": "http://accroche-toi-a-ton-sloup.over-blog.com/",
                "xmlUrl": "http://accroche-toi-a-ton-sloup.over-blog.com/rss",
                "title": "sans titre"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "52000",
                "type": "rss",
                "htmlUrl": "http://alrik.canalblog.com/",
                "xmlUrl": "http://alrik.canalblog.com/rss.xml",
                "title": "Alrik le Geek"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "53000",
                "type": "rss",
                "htmlUrl": "http://giraultsylvain.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/8524769680322477384/posts/default",
                "title": "msieu Sylvain"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "54000",
                "type": "rss",
                "htmlUrl": "http://clement.over-blog.com/",
                "xmlUrl": "http://clement.over-blog.com/rss-articles.xml",
                "title": "Le blog de -clement-"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "55000",
                "type": "rss",
                "htmlUrl": "http://www.thomasmathieu.net/blog/index.php",
                "xmlUrl": "http://www.thomasmathieu.net/blog/index.php?feed/atom",
                "title": "Le Blog de Thomas Mathieu POINT NET"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "57000",
                "type": "rss",
                "htmlUrl": "http://wouzitcompagnie.canalblog.com/",
                "xmlUrl": "http://wouzitcompagnie.canalblog.com/rss.xml",
                "title": "wouzit"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "58000",
                "type": "rss",
                "htmlUrl": "https://festival-bd-lyon.blog4ever.com/",
                "xmlUrl": "http://static.blog4ever.com/2010/06/418009/rss_articles.xml",
                "title": "Blog..."
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "59000",
                "type": "rss",
                "htmlUrl": "http://maadiarcochon.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/5989431302710363583/posts/default",
                "title": "le blog BD de Maadiar&#x2F; Маадяр"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "60000",
                "type": "rss",
                "htmlUrl": "http://www.destrucs.net/",
                "xmlUrl": "http://www.destrucs.net/rss",
                "title": "Bambiii Des trucs dessinés"
              }
            },
            {
              "@attributes": {
                "col": "4",
                "row": "61000",
                "type": "rss",
                "htmlUrl": "http://bapbd.blogspot.com/",
                "xmlUrl": "http://www.blogger.com/feeds/3084644765364696544/posts/default",
                "title": "Ca me turlute ...."
              }
            }
          ],
          "#text": "",
          "@attributes": {
            "layout": "4-0",
            "cols": "4",
            "title": "Blogs"
          }
        },
        {
          "outline": [
            {
              "@attributes": {
                "col": "1",
                "row": "125",
                "type": "rss",
                "htmlUrl": "https://scienceetonnante.com/",
                "xmlUrl": "https://sciencetonnante.wordpress.com/feed/",
                "title": "Science étonnante » Flux"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "250",
                "type": "rss",
                "htmlUrl": "https://www.arretsurimages.net/",
                "xmlUrl": "https://api.arretsurimages.net/api/public/rss/all-content",
                "title": "Flux RSS d'Arrêt sur Images"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "500",
                "type": "rss",
                "htmlUrl": "https://theierecosmique.com/",
                "xmlUrl": "https://theierecosmique.com/feed/",
                "title": "La Théière Cosmique » Flux"
              }
            },
            {
              "@attributes": {
                "col": "1",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://www.youtube.com/channel/UC5X4e8ScZI2AFd_vkjSoyoQ",
                "xmlUrl": "https://www.youtube.com/feeds/videos.xml?channel_id=UC5X4e8ScZI2AFd_vkjSoyoQ",
                "title": "AstronoGeek"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "500",
                "type": "rss",
                "htmlUrl": "http://chevrepensante.fr/",
                "xmlUrl": "http://chevrepensante.fr/feed/",
                "title": "Chèvre Pensante » Flux"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://www.youtube.com/channel/UCnf0fDz1vTYW-sl36wbVMbg",
                "xmlUrl": "https://www.youtube.com/feeds/videos.xml?channel_id=UCnf0fDz1vTYW-sl36wbVMbg",
                "title": "Les Revues du Monde"
              }
            },
            {
              "@attributes": {
                "col": "2",
                "row": "2000",
                "type": "rss",
                "htmlUrl": "https://sciencepop.fr/",
                "xmlUrl": "https://sciencepop.fr/feed/",
                "title": "Science Pop » Flux"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "250",
                "type": "rss",
                "htmlUrl": "http://www.bunkerd.fr/",
                "xmlUrl": "http://www.bunkerd.fr/feed/",
                "title": "Bunker D » Flux"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "500",
                "type": "rss",
                "htmlUrl": "https://www.afis.org/",
                "xmlUrl": "https://www.pseudo-sciences.org/rss.xml",
                "title": "Afis Science - Association française pour l'information scientifique"
              }
            },
            {
              "@attributes": {
                "col": "3",
                "row": "1000",
                "type": "rss",
                "htmlUrl": "https://www.youtube.com/channel/UCq-8pBMM3I40QlrhM9ExXJQ",
                "xmlUrl": "https://www.youtube.com/feeds/videos.xml?channel_id=UCq-8pBMM3I40QlrhM9ExXJQ",
                "title": "La Tronche en Biais"
              }
            }
          ],
          "#text": "",
          "@attributes": {
            "layout": "3-0",
            "cols": "3",
            "title": "Skeptik"
          }
        }
      ],
      "#text": ""
    },
    "head": {
      "creationDate": {
        "#text": "2021-02-05 06:56:35"
      },
      "type": {
        "#text": "Private"
      },
      "title": {
        "#text": "Nasjoe"
      },
      "#text": ""
    },
    "#text": "",
    "@attributes": {
      "version": "1.0"
    }
  }
};

// var all = objson.opml.body.outline,
//     allTabs = [],
//     thisTabA = [];

// for (var i in all) {

//   var tabs = all[i];
//   var feeds = tabs.outline;

//   for (var key in tabs.outline[0]) {
//     var tab = tabs[key];
//     var thisTab = {};
//     // console.log('This tab: %s', tab.title);
//     // thisTab.cols = tab.cols;
//     var thisTabFeeds = [];

//     for (var feed in feeds) {
//       var thisFeed = {};
//       // console.log('This feed: %s (%s)', feeds[feed]["@attributes"]);
//       thisFeed.url = feeds[feed]["@attributes"].xmlUrl;
//       thisFeed.col = feeds[feed]["@attributes"].col;
//       thisTabFeeds.push(thisFeed);
//     }
//     thisTab.columns = thisTabFeeds;
//     thisTab.cols = tab.cols;
//     thisTab.name = tab.title;
//   }
//   allTabs.push(thisTab);
// }

// console.log('allTabs: %s (%s)', JSON.stringify(allTabs));

var all = objson.opml.body.outline,
    allTabs = [];

for (var i in all) {
  var tabs = all[i],
      feeds = tabs.outline;

  for (var key in tabs.outline[0]) {
    var tab = tabs[key],
        thisTab = {},
        thisTabFeeds = [],
        thisColFeeds = [];

    for (var i in feeds) {
      var thisFeed = {};

      thisFeed.status = "on";
      thisFeed.limit = 6;
      thisFeed.type = "mixed";
      thisFeed.url = feeds[i]["@attributes"].xmlUrl;
      thisIndex = Number(feeds[i]["@attributes"].col) - 1;
      if (!thisColFeeds[thisIndex]) thisColFeeds[thisIndex] = [];
      thisColFeeds[thisIndex].push(thisFeed);
    }
    thisTabFeeds.push(thisColFeeds);
    thisTab.columns = thisColFeeds;
    thisTab.name = tab.title;
  }
  allTabs.push(thisTab);
}

console.log('allTabs: %s (%s)', JSON.stringify(allTabs));
